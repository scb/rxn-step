This is COMSOL Multiphysics code used for the following works:
1. E. Earl and S. Calabrese Barton, "Simulation of intermediate transport in nanoscale scaffolds for multistep catalytic reactions", Phys. Chem. Chem. Phys., 19, 15463–15470 (2017). [doi:10.1039/C7CP00239D](//doi.org/10.1039/C7CP00239D)
1. E. Earl, "Intermediate Transport in Nanoscale Scaffolds for Multistep Catalytic Reactions" Masters Thesis, Michigan State University (2016). [link](http://catalog.lib.msu.edu/record=b12269154~S39a)

The following code can be used to create figures that appear in these works:
 * [Commit 1](https://gitlab.msu.edu/scb/rxn-step/commit/26a5454b78e4ed38004e94d5ff1423a2928aba2b): Base Model Figures.
 * [Commit 2](https://gitlab.msu.edu/scb/rxn-step/commit/0cf826eb690463bbd1804b1f03c92c6e7abdb14a): Electrostatic Model Figures.
 * [Commit 3](https://gitlab.msu.edu/scb/rxn-step/commit/3c5bb86798e0dea9017cce643385c3916e537b7b): Adsorption
 * [Commit 4](https://gitlab.msu.edu/scb/rxn-step/commit/1effdcec45829a929d05e72270117520418f1d82): Tau figure. 
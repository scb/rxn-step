
function phi=potfun(pos,Q,posQ)

%%
% pos is desired position in cartesian coordinates
% Q is a vector of N point charges
% posQ is a (N x 3) matrix of coordinates for each point charge

%% Example
% pos= [1 0 0];
% Q=   [1 1 1];
% posQ=[0  1 0;
%       0  0 0;
%       0 -1 0];
% potfun([1 0 0],Q,posQ)   => 2.412

%% calculate distances from point charges

N=length(Q);
d=sqrt(sum([(posQ-repmat(pos,N,1)).^2]'));

%% Calculate potential
b=0.7; 
k=1.38*10^-23;
e=1.60*10^-19;
T=298;
phi=sum(Q.*b./d).*k.*T./e;





%% Data
% As an example, choose three point charges on the y axis.

Q=   [1 1 1];
a=length(Q);
x=zeros(1,a);
y=[-2.5:5/(a-1):2.5];
posQ=[x;y;x]'


%%

xv=linspace(0.1,2);
yv=linspace(-3,3);

Nx=length(xv);
Ny=length(yv);
phi=zeros(Nx,Ny);

for ix=1:length(xv)
    for iy=1:length(yv)
        pos=[xv(ix) yv(iy) 0];
        phi(ix,iy)=potfun(pos,Q,posQ);
    end % for iy
end % for ix

%%
surf(yv,xv,phi)
xlabel('y (nm)')
ylabel('x (nm)')
zlabel('Potential (V)')
grid on
set(gca,'GridLineStyle','-')

view(-212.5,18)
%%
phi(3,4)
ModelFileName='half_circle.mph';

%%

model=mphload(ModelFileName);
model.param.set('D', 1);
model.param.set('k0', 1);
model.param.set('kad', 1);
model.param.set('kd', 1);
model.param.set('K', 1e10);
model.study('std2').run;
tabl=model.result.table('tbl6').getReal;
%ModelUtil.remove('model')

%%
model=mphload(ModelFileName);
model.param.set('D', 1.1);
model.param.set('k0', 1);
model.param.set('kad', 1);
model.param.set('kd', 1);
model.param.set('K', 1e10);
model.study('std2').run;
tabl2=model.result.table('tbl6').getReal;

%%
model=mphload(ModelFileName);
model.param.set('D', 1);
model.param.set('k0', 1.1);
model.param.set('kad', 1);
model.param.set('kd', 1);
model.param.set('K', 1e10);
model.study('std2').run;
tabl3=model.result.table('tbl6').getReal;

%%

model=mphload(ModelFileName);
model.param.set('D', 1);
model.param.set('k0', 1);
model.param.set('kad', 1.1);
model.param.set('kd', 1);
model.param.set('K', 1e10);
model.study('std2').run;
tabl4=model.result.table('tbl6').getReal;

%%

model=mphload(ModelFileName);
model.param.set('D', 1);
model.param.set('k0', 1);
model.param.set('kad', 1);
model.param.set('kd', 1.1);
model.param.set('K', 1e10);
model.study('std2').run;
tabl5=model.result.table('tbl6').getReal;

%%

D2=tabl(:,1);
y=tabl(:,8);
Dy=tabl2(:,8);
k0y=tabl3(:,8);
kady=tabl4(:,8);
kdy=tabl5(:,8);

dy= diff (y) ./ diff(D2);
fcc= D2 (1:end-1) ./ y(1:end-1) .* dy;

dkady= (kady-y) ./ 0.1;
dk0y= (k0y-y) ./ 0.1;
dDy= (Dy-y) ./ 0.1;
dkdy=(kdy-y) ./ 0.1;

kadfcc= dkady .* 1 ./ y;
k0fcc= dk0y .*1 ./ y;
Dfcc= dDy .*1 ./ y;
kdfcc= dkdy .*1 ./ y;

% semilogx(D2,y,'c',D2,kady,'-m',D2,kdy,'-r',D2,k0y,'-b',D2,Dy,'-g')


%%
semilogx(D2(1:end-1), fcc, '-c', D2, kadfcc, '-m', D2,kdfcc,'-b',D2,k0fcc,'-r',D2,Dfcc,'-g')
xlabel('\phi')
ylabel('fcc')
legend('\phi','k_{ad}', 'k_d', 'k_0', 'D','Location','Northeast')

%%
out=[D2,y,k0y,kady,kdy,Dy];
out2=[k0fcc,kadfcc,kdfcc,Dfcc];






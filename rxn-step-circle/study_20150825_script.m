

%% make all the figures docked
set(0,'DefaultFigureWindowStyle','docked')

%% load model
ModelFileName='half_circle.mph';
model=mphload(ModelFileName);

%% tweak parameters
model.param.set('K',1);
model.param.set('D',1000);
model.param.set('kad',100);
%model.param.set('Ka',0.01);

%% Display parameters
mphgetexpressions(model.param)

%% define study
resultstr='bnd13';
studystr='std2';
tablestr='tbl6';

%%
model.param.set('Ka',0.01);
model.study(studystr).run
table0p01=mphtable(model,tablestr);

%%
model.param.set('Ka',0.1);
model.study(studystr).run
table0p1=mphtable(model,tablestr);

%%
model.param.set('Ka',1);
model.study(studystr).run
table1=mphtable(model,tablestr);

%%
model.param.set('Ka',10);
model.study(studystr).run
table10=mphtable(model,tablestr);

%%
model.param.set('Ka',100);
model.study(studystr).run
table100=mphtable(model,tablestr);

%% Flux plots
figure(2); clf
x0=table1.data(:,1);
r0p01= table0p01.data(:,7) ./ table0p01.data(:,9);
r0p1=  table0p1.data(:,7) ./ table0p1.data(:,9);
r1=    table1.data(:,7) ./ table1.data(:,9);
r10=   table10.data(:,7) ./ table10.data(:,9);
r100=  table100.data(:,7) ./ table100.data(:,9);

ra0p01= table0p01.data(:,7);
ra0p1=  table0p1.data(:,7);
ra1=    table1.data(:,7);
ra10=   table10.data(:,7); 
ra100=  table100.data(:,7);

rd1= [r0p01,r0p1, r1,r10, r100];
rad1=[ra0p01,ra0p1, ra1,ra10, ra100];

semilogx(x0,rd1)
xlabel('k_1')
ylabel('Flux / mol cm^{-2}')
legend('K_a=0.01','K_a=0.1','K_a=1','K_a=10','K_a=100','Location','Southeast')
title('Relative Flux')

%%
semilogx(x0,rad1)
xlabel('k_1')
ylabel('Flux / mol cm^{-2}')
legend('K_a=0.01','K_a=0.1','K_a=1','K_a=10','K_a=100','Location','Southeast')
title('Absolute Flux')

%% Run with Da=2D
model.param.set('Da','2*D');

model.param.set('Ka',0.01);
model.study(studystr).run
table0p01=mphtable(model,tablestr);

model.param.set('Ka',0.1);
model.study(studystr).run
table0p1=mphtable(model,tablestr);

model.param.set('Ka',1);
model.study(studystr).run
table1=mphtable(model,tablestr);

model.param.set('Ka',10);
model.study(studystr).run
table10=mphtable(model,tablestr);

model.param.set('Ka',100);
model.study(studystr).run
table100=mphtable(model,tablestr);

%% Flux plots
figure(2); clf
x0=table1.data(:,1);
r0p01= table0p01.data(:,7) ./ table0p01.data(:,9);
r0p1=  table0p1.data(:,7) ./ table0p1.data(:,9);
r1=    table1.data(:,7) ./ table1.data(:,9);
r10=   table10.data(:,7) ./ table10.data(:,9);
r100=  table100.data(:,7) ./ table100.data(:,9);

ra0p01= table0p01.data(:,7);
ra0p1=  table0p1.data(:,7);
ra1=    table1.data(:,7);
ra10=   table10.data(:,7); 
ra100=  table100.data(:,7);

rd2= [r0p01,r0p1, r1,r10, r100];
rad2=[ra0p01,ra0p1, ra1,ra10, ra100];

semilogx(x0,[r0p01,r0p1, r1,r10, r100])
xlabel('k_1')
ylabel('Flux / mol cm^{-2}')
legend('K_a=0.01','K_a=0.1','K_a=1','K_a=10','K_a=100','Location','Southeast')
title('Relative Flux, D_a=2D')

%%
semilogx(x0,[ra0p01,ra0p1, ra1,ra10, ra100])
xlabel('k_1')
ylabel('Flux / mol cm^{-2}')
legend('K_a=0.01','K_a=0.1','K_a=1','K_a=10','K_a=100','Location','Southeast')
title('Absolute Flux, D_a=2D')

%% Run with Da=4D
model.param.set('Da','4*D');

model.param.set('Ka',0.01);
model.study(studystr).run
table0p01=mphtable(model,tablestr);

model.param.set('Ka',0.1);
model.study(studystr).run
table0p1=mphtable(model,tablestr);

model.param.set('Ka',1);
model.study(studystr).run
table1=mphtable(model,tablestr);

model.param.set('Ka',10);
model.study(studystr).run
table10=mphtable(model,tablestr);

model.param.set('Ka',100);
model.study(studystr).run
table100=mphtable(model,tablestr);

%% Flux plots
figure(2); clf
x0=table1.data(:,1);
r0p01= table0p01.data(:,7) ./ table0p01.data(:,9);
r0p1=  table0p1.data(:,7) ./ table0p1.data(:,9);
r1=    table1.data(:,7) ./ table1.data(:,9);
r10=   table10.data(:,7) ./ table10.data(:,9);
r100=  table100.data(:,7) ./ table100.data(:,9);

ra0p01= table0p01.data(:,7);
ra0p1=  table0p1.data(:,7);
ra1=    table1.data(:,7);
ra10=   table10.data(:,7); 
ra100=  table100.data(:,7);

rd4= [r0p01,r0p1, r1,r10, r100];
rad4=[ra0p01,ra0p1, ra1,ra10, ra100];

semilogx(x0,[r0p01,r0p1, r1,r10, r100])
xlabel('k_1')
ylabel('Flux / mol cm^{-2}')
legend('K_a=0.01','K_a=0.1','K_a=1','K_a=10','K_a=100','Location','Southeast')
title('Relative Flux, D_a=4D')

%%
semilogx(x0,[ra0p01,ra0p1, ra1,ra10, ra100])
xlabel('k_1')
ylabel('Flux / mol cm^{-2}')
legend('K_a=0.01','K_a=0.1','K_a=1','K_a=10','K_a=100','Location','Southeast')
title('Absolute Flux, D_a=4D')

%%
semilogx(x0,rad1(:,[2 3 4]),x0, rad2(:,[2 3 4]),'--', x0, rad4(:,[2 3 4]),':')
function [fcc,table0,y0,p0,x0]=mph_fcc(model, paramstr, resultstr, studystr, tablestr)

    if ~exist('studystr',  'var')
        studystr=  'std1'; 
    end
    
    if ~exist('resultstr', 'var') 
        resultstr='bnd13'; 
    end
    
    if ~exist('tablestr', 'var')
        tablestr='tbl6'; 
    end
    
    %%
%     ModelFileName='half_circle.mph';
%     model=mphload(ModelFileName);
%     
%     paramstr='k0';
%     studystr='std1';
%     resultstr='bnd13';
%     

    %% 
    % Determine whether study is a parametric sweep
    
    StudyFields=mphmodel(model.study(studystr));
    IsSweep=isfield(StudyFields,'param');
    
    %%
    % fcc vector from sweep study
    % resultstr must be a probe tag
    
    if IsSweep
        
        % check that resulstr is a probe tag
        % ptags=model.probe.tags();
        % idx=strfind(cell(x),resultstr)
        % need to finish this
        
        % Get swept parameter of sweep study
        sdtprop=mphgetproperties(model.study(studystr).feature('param'));
        SweepParam=sdtprop.pname;
                
        %% Baseline parameter and result values
        model.study(studystr).run;
        table0=mphtable(model,tablestr);

        p0=mphevaluate(model,paramstr);
        x0=table0.data(:,1);            
        
        % find column of resultstr
        headerstr=model.probe(resultstr).label();
        idx=strfind(table0.headers,headerstr);
        ResultColumn=find(cellfun(@(x) ~isempty(x),idx)==1);

        y0=table0.data(:,ResultColumn);

        %% Perturbed parameter and result values
        p1=p0*1.05;
        model.param.set(paramstr,p1);
        model.study(studystr).run;
        table1=mphtable(model,tablestr);

        x1=table1.data(:,1);            
       
        % find column of resultstr
        headerstr=model.probe('bnd13').label();
        idx=strfind(table1.headers,headerstr);
        ResultColumn=find(cellfun(@(x) ~isempty(x),idx)==1);

        y1=table1.data(:,ResultColumn);

        %% Calculate fcc
    
        if strcmp(paramstr,SweepParam)
            dydx= (y1-y0) ./ (x1-x0);
            fcc= x0./y0 .* dydx;
        else
            dydp= (y1-y0) ./ (p1-p0);
            fcc= p0./y0 .* dydp;
        end %if 
        
    else %if not a sweep

        %% Baseline parameter and result values
        model.study(studystr).run;

        p0=mphglobal(model,paramstr);
        x0=p0;
        y0=mphglobal(model,resultstr);

        %% Modified parameter and result values

        p1=p0*1.05;
        x1=p1;
        model.param.set(paramstr,p1);
        model.study(studystr).run;

        p1=mphglobal(model,paramstr);
        y1=mphglobal(model,resultstr);

        %% Calculate fcc

        dydp= (y1-y0) / (p1-p0);
        fcc= p0/y0 * dydp;
        
        % make a dummy table
        table0=[x0 y0];

   end %if IsSweep

   % reset the parameter back to its original value
   model.param.set(paramstr,p0);     



    

    
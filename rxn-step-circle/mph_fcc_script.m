%% mph_fcc_script

%% make all the figures docked
set(0,'DefaultFigureWindowStyle','docked')

%% load model
ModelFileName='half_circle.mph';
model=mphload(ModelFileName);

%% tweak parameters
model.param.set('K',1);
%model.param.set('kad',1000);
model.param.set('Ka',0.01);

%% define study
resultstr='bnd13';
studystr='std2';
tablestr='tbl6';

%%
[ck1,table0,y0,k1,x0]=mph_fcc(model,'k1', resultstr, studystr, tablestr);
ck0=mph_fcc(model,'k0', resultstr, studystr, tablestr);
ckad=mph_fcc(model,'kad', resultstr, studystr, tablestr);
cD=mph_fcc(model,'D', resultstr, studystr, tablestr);

%%
c=[ck0,ck1,ckad,cD];
csum=sum(c,2);

%% FCCs

figure(1)
semilogx(x0,[c, csum] )
xlabel('k_1')
ylabel('fcc')
legend( 'k_0', 'k_1', 'k_{ad}', 'D', 'sum', 'Location', 'Northeast')
title('FCCs')

%% Flux plots
figure(2)
r0=table0.data(:,9);
r1=table0.data(:,7);

% absolute
subplot(2,1,1)
semilogx(x0,r0,x0,r1)
xlabel('k_1')
ylabel('Flux / mol cm^{-2}')
legend('r_0','r_1','Location','Southeast')
title('Absolute Flux')

% relative
subplot(2,1,2)
semilogx(x0,r1./r0)
xlabel('k_1')
ylabel('r_1/r_0')
legend('r_0/r_1','Location','Southeast')
title('Relative Flux')

%% Mole Balance

data=table0.data;
balance=(data(:,9)-data(:,7)-data(:,11) ) ./ data(:,9) * 100;

figure(3);clf
semilogx(x0,balance)
xlabel('k_1')
ylabel('Mole Balance Error / %')
